//Imports de los módulos
const fs = require("fs/promises");
const path = require("path");
const usersPath = path.resolve("users.json");
const helloPath = path.resolve("hello.txt");

const readFileUsers = () => {
  //Imprimir en consola el arreglo de usuarios
  fs.readFile(usersPath, "utf8")
    .then((raw) => {
      console.log(raw);
    })
    .catch((error) => {
      console.error(error);
    });
};

const writeHelloWorld = () => {
  //Escribir hello world! en el archivo hello.txt
  fs.writeFile(helloPath, "hello world!").catch((error) => {
    console.error(error);
  });
};

const addUser = async(username) => {
  //Agregar un usuario en la lista users.json
    try{
        usuarios = await fs.readFile(usersPath, "utf-8")
        return await fs.writeFile(usersPath, JSON.stringify(JSON.parse(usuarios).concat(username)))
    }
    catch(error){
        console.error(error)
    }


};

//No hace falta ejecutar las funciones

module.exports = {
  readFileUsers,
  writeHelloWorld,
  addUser,
};
